﻿using System;

namespace ConsoleApp81
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input Number = ");
            int n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($" the Cub of {n} = {n*n*n}");
        }
    }
}
